package Read;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.io.FileNotFoundException;
import java.io.IOException;
//import Process.*;
//import Writter.*;
import Sales.*;

public class Read extends  SalesTax2Main

{
  public List<String[]> readFile(String FileName) throws IOException ,FileNotFoundException,NumberFormatException
    {
        String line = "";
        BufferedReader fileReader = null;
        fileReader = new BufferedReader(new FileReader(FileName));
        final List<String[]> data = new ArrayList<>();
        int counter = 0;
        while ((line = fileReader.readLine()) != null) 
        {
            if (counter != 0) {
                final String[] tokens = line.split(",");
                int arrLength=tokens.length;
                if(arrLength>4)
                 {
                     System.out.println("invalid data in csv file");
                     break;
                 }
                data.add(tokens);
            }
        counter++;
       // System.out.println(data);
    
        //System.out.println("d");
        
        }
        fileReader.close();
        return data;
    }

}
