
package Writter;


import java.io.FileWriter;
import java.util.List;
import java.io.FileNotFoundException;
import java.io.IOException;
import Read.*;
//import Process.*;

public class Writter extends Read
{
  public void writte(final List<String[]> newList) throws IOException ,FileNotFoundException,NumberFormatException
    {
        FileWriter csvWriter = new FileWriter("SalesTax2output.csv");
        String[] data1={"product-name,"," ","cost-price,"," ","sales-tax,"," ","final-price,"," ","country"};
        for(int header=0; header<data1.length; header++)
        {
            csvWriter.append(String.join(" , ", data1[header]));
        }
        csvWriter.append("\n");
        for (final String[] rowData : newList)
        {
            csvWriter.append(String.join(" , ", rowData));
            csvWriter.append("\n");
        }

        csvWriter.flush();
        csvWriter.close();
    }
}
       