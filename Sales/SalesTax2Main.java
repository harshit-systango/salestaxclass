package Sales;
import java.io.FileNotFoundException;
import java.util.List;
import java.io.IOException;
import Process.*;
import Read.*;
import Writter.*;


public class SalesTax2Main
{
    public static void main(final String[] args)
    {

        String FileName="SalesTax2input.csv";
         try
            {       
                if((args.length==1))
                {
                FileName=args[0]; 
                }
                final Read r1 = new Read();
                List<String[]>R1=r1.readFile(FileName);

                processing p1=new processing();
                List<String[]>  R2=p1.process(R1);
               
                Writter w1=new Writter();
                w1.writte(R2);
            }
                catch (FileNotFoundException e) 
                {
                    System.out.println("no file found");
                } 
                catch (IOException e) 
                {
                    System.out.println("input output error ");
                }  
                catch (NumberFormatException e) 
                    {
                        System.out.println("data wrong ");
                    }      
   
    }
    
}
